<?php

declare(strict_types=1);

namespace App\Bundle\Recaptcha;

/**
 * Google reCAPTCHA configuration
 *
 * TODO: Since PHP 8.1 all props should be marked as readonly.
 *
 * @link https://www.google.com/recaptcha/admin/create
 */
final class RecaptchaConfiguration
{

    /**
     * Constructor of the class
     *
     * @param string $publicKey
     * @param string $privateKey
     */
    public function __construct(
        public string $publicKey,
        public string $privateKey,
    ) {
    }
}
