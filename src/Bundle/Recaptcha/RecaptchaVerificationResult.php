<?php

declare(strict_types=1);

namespace App\Bundle\Recaptcha;

/**
 * Import classes
 */
use DateTimeImmutable;
use Sunrise\Hydrator\Annotation\Alias;

/**
 * Google reCAPTCHA verification result
 *
 * TODO: Since PHP 8.1 all props should be marked as readonly.
 *
 * @link https://developers.google.com/recaptcha/docs/v3
 * @link https://developers.google.com/recaptcha/docs/verify#api-response
 * @link https://developers.google.com/recaptcha/docs/verify#error_code_reference
 */
final class RecaptchaVerificationResult
{

    /**
     * Whether this request was a valid reCAPTCHA token for your site
     *
     * @var bool
     */
    #[Alias('success')]
    public bool $isSuccess = false;

    /**
     * The score for this request (0.0 - 1.0)
     *
     * @var float|null
     */
    public ?float $score = null;

    /**
     * The action for this request
     *
     * @var string|null
     */
    public ?string $action = null;

    /**
     * The hostname of the site where the reCAPTCHA was solved
     *
     * @var string|null
     */
    public ?string $hostname = null;

    /**
     * Timestamp of the challenge load
     *
     * @var DateTimeImmutable|null
     */
    #[Alias('challenge_ts')]
    public ?DateTimeImmutable $challengeAt = null;

    /**
     * List of error codes
     *
     * @var string[]
     */
    #[Alias('error-codes')]
    public array $errorCodes = [];
}
