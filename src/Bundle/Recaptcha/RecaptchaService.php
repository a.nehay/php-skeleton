<?php

declare(strict_types=1);

namespace App\Bundle\Recaptcha;

/**
 * Import classes
 */
use Sunrise\Http\Client\Curl\Client;
use Sunrise\Http\Factory\RequestFactory;
use Sunrise\Http\Factory\ResponseFactory;
use Sunrise\Hydrator\Hydrator;

/**
 * Google reCAPTCHA service
 */
final class RecaptchaService
{

    /**
     * Constructor of the class
     *
     * @param RecaptchaConfiguration $configuration
     */
    public function __construct(
        private RecaptchaConfiguration $configuration,
    ) {
    }

    /**
     * Verifies the given token
     *
     * @param string $token
     * @param string $remoteIp
     *
     * @return RecaptchaVerificationResult
     */
    public function verify(string $token, string $remoteIp = '') : RecaptchaVerificationResult
    {
        $uri = 'https://www.google.com/recaptcha/api/siteverify?';
        $uri .= '&secret=' . $this->configuration->privateKey;
        $uri .= '&response=' . $token;
        $uri .= '&remoteip=' . $remoteIp;

        $client = new Client(new ResponseFactory(), []);
        $request = (new RequestFactory)->createRequest('POST', $uri);
        $response = $client->sendRequest($request);

        /** @var RecaptchaVerificationResult */
        return (new Hydrator)->hydrateWithJson(
            new RecaptchaVerificationResult(),
            (string) $response->getBody()
        );
    }
}
