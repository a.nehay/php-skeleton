<?php

declare(strict_types=1);

namespace App\Bundle\Telegram;

/**
 * Import classes
 */
use App\Service\TokenService;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Sunrise\Http\Client\Curl\Client;
use Sunrise\Http\Message\RequestFactory;
use Sunrise\Http\Message\ResponseFactory;
use Sunrise\Hydrator\Exception\InvalidValueException;
use Sunrise\Hydrator\Hydrator;
use Generator;
use Throwable;

/**
 * Import functions
 */
use function file_exists;
use function file_get_contents;
use function file_put_contents;
use function http_build_query;
use function json_encode;
use function sprintf;

/**
 * Import constants
 */
use const CURLOPT_CONNECTTIMEOUT;

/**
 * Telegram client
 */
final class TelegramClient
{
    private ClientInterface $httpClient;
    private RequestFactoryInterface $requestFactory;
    private ResponseFactoryInterface $responseFactory;

    /**
     * Constructor of the class
     *
     * @param TokenService $tokenService
     * @param TelegramConfiguration $configuration
     * @param LoggerInterface $logger
     */
    public function __construct(
        private TokenService $tokenService,
        private TelegramConfiguration $configuration,
        private LoggerInterface $logger,
    ) {
        $this->requestFactory = new RequestFactory();
        $this->responseFactory = new ResponseFactory();

        $this->httpClient = new Client($this->responseFactory, [
            CURLOPT_TIMEOUT => 10,
        ]);
    }

    public function buildMethodUri(string $methodName, ?array $queryParams = null) : string
    {
        $methodUri = sprintf('https://api.telegram.org/bot%s/%s', $this->configuration->botToken, $methodName);

        if (isset($queryParams)) {
            $methodUri .= '?' . http_build_query($queryParams);
        }

        return $methodUri;
    }

    public function callMethod(string $methodName, ?array $queryParams = null, ?array $payload = null) : ResponseInterface
    {
        $methodUri = $this->buildMethodUri($methodName, $queryParams);

        if (isset($payload)) {
            $request = $this->requestFactory->createRequest('POST', $methodUri);
            $request = $request->withHeader('Content-Type', 'application/json');
            $request->getBody()->write(json_encode($payload));
        } else {
            $request = $this->requestFactory->createRequest('GET', $methodUri);
        }

        return $this->httpClient->sendRequest($request);
    }

    /**
     * Sends a message
     *
     * @param int $chatId
     * @param string $html
     *
     * @return int
     *
     * @see https://core.telegram.org/bots/api#sendmessage
     */
    public function sendMessage(int $chatId, string $html) : int
    {
        $message = [];
        $message['chat_id'] = $chatId;
        $message['text'] = $html;
        $message['parse_mode'] = 'HTML';

        $response = $this->callMethod('sendMessage', payload: $message);

        return $response->getStatusCode();
    }

    /**
     * Sends an invoice
     *
     * @param int $chatId
     *
     * @return mixed
     */
    public function sendInvoice(int $chatId)
    {
        $message = [];
        $message['chat_id'] = $chatId;
        $message['title'] = 'a title of the product';
        $message['description'] = 'a description of the product';
        $message['payload'] = $this->tokenService->generateRandomString(128);
        $message['start_parameter'] = $this->tokenService->generateRandomString(32);
        $message['provider_token'] = '381764678:TEST:34581';
        $message['currency'] = 'RUB';
        $message['prices'][0]['label'] = 'a label of the price';
        $message['prices'][0]['amount'] = 100 * 100;
        $message['total_amount'] = $message['prices'][0]['amount'];
        $message['need_email'] = true;
        $message['send_email_to_provider'] = true;
        $message['provider_data']['receipt']['items'][] = [
            'description' => 'a description (summary) of the product',
            'quantity' => '1.00',
            'amount' => [
                'value' => '100.00',
                'currency' => 'RUB',
            ],
            'vat_code' => 1,
        ];

        $response = $this->callMethod('sendInvoice', payload: $message);

        return (string) $response->getBody();
    }

    public function answerPreCheckoutQuery(Dto\PreCheckoutQuery $preCheckoutQuery)
    {
        $message = [];
        $message['pre_checkout_query_id'] = $preCheckoutQuery->id;
        $message['ok'] = true;
        // $message['error_message'] = '';

        $response = $this->callMethod('answerPreCheckoutQuery', payload: $message);

        return (string) $response->getBody();
    }

    /**
     * Consumes the application bot's updates
     *
     * @return Generator
     *
     * @link https://core.telegram.org/bots/api#getting-updates
     */
    public function consumeUpdates() : Generator
    {
        $offset = null;
        $offsetFile = $this->configuration->offsetFile;
        if (file_exists($offsetFile)) {
            $offset = (int) file_get_contents($offsetFile);
        }

        $hydrator = new Hydrator();

        while (true) {
            $this->logger->debug(sprintf('Current offset: %d', $offset ?? 0), [
                'timestamp' => \time(),
            ]);

            try {
                $response = $this->callMethod('getUpdates', queryParams: [
                    'offset' => $offset,
                ]);
            } catch (ClientExceptionInterface $e) {
                $this->logger->warning(sprintf('Something went wrong: %s', $e->getMessage()));
                continue;
            }

            $statusCode = $response->getStatusCode();
            if ($statusCode <> 200) {
                $this->logger->warning(sprintf('Unexpected status code: %d', $statusCode));
                continue;
            }

            $json = (string) $response->getBody();

            try {
                $updates = $hydrator->hydrateWithJson(Dto\Updates::class, $json);
            } catch (InvalidValueException $e) {
                $this->logger->warning(sprintf('Unexpected payload: %s', $e->getMessage()));
                continue;
            }

            if ($updates->result->isEmpty()) {
                $this->logger->debug('No updates');
                continue;
            }

            foreach ($updates->result->all() as $update) {
                $this->logger->debug('A new update was received', [
                    'update' => $update,
                ]);

                try {
                    yield $update;
                    $offset = $update->update_id + 1;
                    file_put_contents($offsetFile, $offset);
                    $this->logger->debug('Update was accepted');
                } catch (Throwable $exception) {
                    $this->logger->error('Update was rejected', [
                        'update' => $update,
                        'exception' => $exception,
                    ]);

                    break;
                }
            }
        }
    }
}
