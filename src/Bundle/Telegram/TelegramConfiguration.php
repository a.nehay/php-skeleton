<?php

declare(strict_types=1);

namespace App\Bundle\Telegram;

/**
 * Telegram bundle configuration
 *
 * TODO: Since PHP 8.1 all props should be marked as readonly.
 */
final class TelegramConfiguration
{

    /**
     * Constructor of the class
     *
     * @param string $botName
     * @param string $botToken
     * @param string $offsetFile
     */
    public function __construct(
        public string $botName,
        public string $botToken,
        public string $offsetFile,
    ) {
    }
}
