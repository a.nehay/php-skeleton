<?php

declare(strict_types=1);

namespace App\Bundle\Telegram\Dto;

/**
 * This object represents a response from the telegram API method "getUpdates".
 *
 * @see https://core.telegram.org/bots/api#getupdates
 */
final class Updates
{
    public bool $ok;
    public UpdateCollection $result;
}
