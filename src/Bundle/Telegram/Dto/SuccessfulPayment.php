<?php

declare(strict_types=1);

namespace App\Bundle\Telegram\Dto;

/**
 * This object contains basic information about a successful payment.
 *
 * @see https://core.telegram.org/bots/api#successfulpayment
 */
final class SuccessfulPayment
{
    public string $currency;
    public int $total_amount;
    public string $invoice_payload;
    public ?string $shipping_option_id = null;
    public ?OrderInfo $order_info = null;
    public string $telegram_payment_charge_id;
    public string $provider_payment_charge_id;
}
