<?php

declare(strict_types=1);

namespace App\Bundle\Telegram\Dto;

/**
 * This object represents a Telegram user or bot.
 *
 * @see https://core.telegram.org/bots/api#user
 */
final class User
{
    public int $id;
}
