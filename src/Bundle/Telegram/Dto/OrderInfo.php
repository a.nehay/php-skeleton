<?php

declare(strict_types=1);

namespace App\Bundle\Telegram\Dto;

/**
 * This object represents information about an order.
 *
 * @see https://core.telegram.org/bots/api#orderinfo
 */
final class OrderInfo
{
    public ?string $name = null;
    public ?string $phone_number = null;
    public ?string $email = null;
    public ?ShippingAddress $shipping_address = null;
}
