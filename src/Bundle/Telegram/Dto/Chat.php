<?php

declare(strict_types=1);

namespace App\Bundle\Telegram\Dto;

/**
 * This object represents a chat.
 *
 * @see https://core.telegram.org/bots/api#chat
 */
final class Chat
{
    public int $id;
}
