<?php

declare(strict_types=1);

namespace App\Bundle\Telegram\Dto;

use Sunrise\Hydrator\ObjectCollection;

/**
 * This collection represents updates.
 *
 * @see https://core.telegram.org/bots/api#update
 */
final class UpdateCollection extends ObjectCollection
{
    public const T = Update::class;
}
