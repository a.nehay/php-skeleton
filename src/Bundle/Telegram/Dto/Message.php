<?php

declare(strict_types=1);

namespace App\Bundle\Telegram\Dto;

/**
 * This object represents a message.
 *
 * @see https://core.telegram.org/bots/api#message
 */
final class Message
{
    public int $message_id;
    public Chat $chat;
    public ?User $from = null;
    public ?string $text = null;
    public ?SuccessfulPayment $successful_payment = null;
}
