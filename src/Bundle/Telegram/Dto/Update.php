<?php

declare(strict_types=1);

namespace App\Bundle\Telegram\Dto;

/**
 * This object represents an incoming update.
 *
 * At most one of the optional parameters can be present in any given update.
 *
 * @see https://core.telegram.org/bots/api#update
 */
final class Update
{
    public int $update_id;
    public ?Message $message = null;
    public ?PreCheckoutQuery $pre_checkout_query = null;
}
