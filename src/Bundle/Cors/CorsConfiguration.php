<?php

declare(strict_types=1);

namespace App\Bundle\Cors;

/**
 * Import functions
 */
use function in_array;

/**
 * CORS configuration
 *
 * TODO: Since PHP 8.1 all props should be marked as readonly.
 *
 * @link https://fetch.spec.whatwg.org/#http-cors-protocol
 * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
 * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Credentials
 * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Headers
 * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Methods
 * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Origin
 * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Max-Age
 */
final class CorsConfiguration
{

    /**
     * Constructor of the class
     *
     * @param bool     $allowCredentials
     * @param string[] $allowedHeaders
     * @param string[] $allowedMethods
     * @param string[] $allowedOrigins
     * @param int      $maxAge
     */
    public function __construct(
        public bool $allowCredentials,
        public array $allowedHeaders,
        public array $allowedMethods,
        public array $allowedOrigins,
        public int $maxAge,
    ) {
    }

    /**
     * Checks if the given origin is allowed
     *
     * @param string $origin
     *
     * @return bool
     */
    public function allowsOrigin(string $origin) : bool
    {
        if (in_array('*', $this->allowedOrigins)) {
            return true;
        }

        if (in_array($origin, $this->allowedOrigins)) {
            return true;
        }

        return false;
    }
}
