<?php

declare(strict_types=1);

namespace App\Bundle\Cors;

/**
 * Import classes
 */
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Sunrise\Http\Factory\ResponseFactory;
use Sunrise\Http\Factory\UriFactory;
use Sunrise\Http\Header\HeaderAccessControlAllowCredentials;
use Sunrise\Http\Header\HeaderAccessControlAllowHeaders;
use Sunrise\Http\Header\HeaderAccessControlAllowMethods;
use Sunrise\Http\Header\HeaderAccessControlAllowOrigin;
use Sunrise\Http\Header\HeaderAccessControlMaxAge;
use Sunrise\Http\Header\HeaderCollection;
use Sunrise\Http\Header\HeaderCollectionInterface;

/**
 * CORS middleware
 *
 * Note that this middleware handles origins only.
 *
 * @link https://fetch.spec.whatwg.org/#http-cors-protocol
 * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
 * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Credentials
 * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Headers
 * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Methods
 * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Origin
 * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Max-Age
 * @link https://github.com/sunrise-php/http-header-kit
 */
final class CorsMiddleware implements MiddlewareInterface
{

    /**
     * Constructor of the class
     *
     * @param CorsConfiguration $configuration
     */
    public function __construct(
        private CorsConfiguration $configuration,
    ) {
    }

    /**
     * {@inheritdoc}
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler) : ResponseInterface
    {
        // CORS request MUST contain the Origin header...
        if (!$request->hasHeader('origin')) {
            return $handler->handle($request);
        }

        // if the origin is unknown, access MUST be denied...
        $origin = $request->getHeaderLine('origin');
        if (!$this->configuration->allowsOrigin($origin)) {
            return (new ResponseFactory)->createResponse(403);
        }

        /** @var iterable<int, string> */
        $header = new HeaderAccessControlAllowOrigin((new UriFactory)->createUri($origin));

        // CORS-preflight request MUST use the OPTIONS method...
        if ('OPTIONS' === $request->getMethod()) {
            return $this->createPreflightResponse()->withHeader(...$header);
        }

        return $handler->handle($request)->withHeader(...$header);
    }

    /**
     * Gets CORS-preflight response headers
     *
     * @return HeaderCollectionInterface
     */
    private function getPreflightResponseHeaders() : HeaderCollectionInterface
    {
        $headers = new HeaderCollection();

        if ($this->configuration->allowCredentials) {
            $headers->add(new HeaderAccessControlAllowCredentials());
        }

        if (!empty($this->configuration->allowedHeaders)) {
            $headers->add(new HeaderAccessControlAllowHeaders(...$this->configuration->allowedHeaders));
        }

        if (!empty($this->configuration->allowedMethods)) {
            $headers->add(new HeaderAccessControlAllowMethods(...$this->configuration->allowedMethods));
        }

        $headers->add(new HeaderAccessControlMaxAge($this->configuration->maxAge));

        return $headers;
    }

    /**
     * Propagates CORS-preflight response headers
     *
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     */
    private function propagatePreflightResponseHeaders(ResponseInterface $response) : ResponseInterface
    {
        foreach ($this->getPreflightResponseHeaders()->all() as $header) {
            /** @var iterable<int, string> $header */

            $response = $response->withHeader(...$header);
        }

        return $response;
    }

    /**
     * Creates CORS-preflight response
     *
     * @return ResponseInterface
     */
    private function createPreflightResponse() : ResponseInterface
    {
        $response = (new ResponseFactory)->createResponse(204);

        return $this->propagatePreflightResponseHeaders($response);
    }
}
