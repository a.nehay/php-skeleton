<?php

declare(strict_types=1);

namespace App\EventListener;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Abstract class for an event listener
 */
abstract class AbstractEventListener
{

    /**
     * Gets an event name
     *
     * @return string
     */
    abstract public function getEventName() : string;

    /**
     * Handles the given event
     *
     * @param Event $event
     *
     * @return void
     */
    abstract public function handleEvent(Event $event) : void;

    /**
     * Invokes the object
     *
     * @param Event $event
     *
     * @return void
     */
    final public function __invoke(Event $event) : void
    {
        $this->handleEvent($event);
    }
}
