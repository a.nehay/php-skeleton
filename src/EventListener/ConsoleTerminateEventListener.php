<?php

declare(strict_types=1);

namespace App\EventListener;

use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleTerminateEvent;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * ConsoleTerminateEventListener
 *
 * @link https://symfony.com/doc/current/components/console/events.html#the-consoleevents-terminate-event
 */
final class ConsoleTerminateEventListener extends AbstractEventListener
{

    /**
     * {@inheritdoc}
     */
    public function getEventName() : string
    {
        return ConsoleEvents::TERMINATE;
    }

    /**
     * {@inheritdoc}
     *
     * @param ConsoleTerminateEvent $event
     */
    public function handleEvent(Event $event) : void
    {
        // some code...
    }
}
