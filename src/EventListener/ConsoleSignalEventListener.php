<?php

declare(strict_types=1);

namespace App\EventListener;

use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleSignalEvent;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * ConsoleSignalEventListener
 *
 * @link https://symfony.com/doc/current/components/console/events.html#the-consoleevents-signal-event
 */
final class ConsoleSignalEventListener extends AbstractEventListener
{

    /**
     * {@inheritdoc}
     */
    public function getEventName() : string
    {
        return ConsoleEvents::SIGNAL;
    }

    /**
     * {@inheritdoc}
     *
     * @param ConsoleSignalEvent $event
     */
    public function handleEvent(Event $event) : void
    {
        // some code...
    }
}
