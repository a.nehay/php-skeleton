<?php

declare(strict_types=1);

namespace App\EventListener;

use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * ConsoleCommandEventListener
 *
 * @link https://symfony.com/doc/current/components/console/events.html#the-consoleevents-command-event
 */
final class ConsoleCommandEventListener extends AbstractEventListener
{

    /**
     * {@inheritdoc}
     */
    public function getEventName() : string
    {
        return ConsoleEvents::COMMAND;
    }

    /**
     * {@inheritdoc}
     *
     * @param ConsoleCommandEvent $event
     */
    public function handleEvent(Event $event) : void
    {
        // some code...
    }
}
