<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Annotation\MapRequestBody;
use ReflectionClass;
use ReflectionMethod;
use Sunrise\Http\Router\Event\RouteEvent;
use Sunrise\Http\Router\RequestHandler\CallableRequestHandler;
use Sunrise\Http\Router\RouteInterface;
use Sunrise\Hydrator\Exception\InvalidValueException;
use Sunrise\Hydrator\Hydrator;
use Symfony\Contracts\EventDispatcher\Event;

use function is_array;

/**
 * RouterRouteEventListener
 */
final class RouterRouteEventListener extends AbstractEventListener
{

    /**
     * {@inheritdoc}
     */
    public function getEventName() : string
    {
        return RouteEvent::NAME;
    }

    /**
     * {@inheritdoc}
     *
     * @param RouteEvent $event
     */
    public function handleEvent(Event $event) : void
    {
        $routeHolder = $this->getRouteHolder($event->getRoute());
        if (!isset($routeHolder)) {
            return;
        }

        $attributes = $routeHolder->getAttributes(MapRequestBody::class);
        if (!isset($attributes[0])) {
            return;
        }

        try {
            $object = (new Hydrator)->hydrateWithJson(
                $attributes[0]->newInstance()->value,
                $event->getRequest()->getBody()->__toString()
            );
        } catch (InvalidValueException $e) {
            // TODO: normalize the exception...
            throw $e;
        }

        $event->setRequest($event->getRequest()->withParsedBody($object));
    }

    /**
     * Gets the given route's holder
     *
     * @param RouteInterface $route
     *
     * @return ReflectionClass|ReflectionMethod|null
     */
    private function getRouteHolder(RouteInterface $route) : ReflectionClass|ReflectionMethod|null
    {
        $handler = $route->getRequestHandler();
        if (!($handler instanceof CallableRequestHandler)) {
            return new ReflectionClass($handler);
        }

        $callback = $handler->getCallback();
        if (is_array($callback)) {
            return new ReflectionMethod(...$callback);
        }

        return null;
    }
}
