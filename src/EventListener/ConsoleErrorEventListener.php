<?php

declare(strict_types=1);

namespace App\EventListener;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleErrorEvent;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * ConsoleErrorEventListener
 *
 * @link https://symfony.com/doc/current/components/console/events.html#the-consoleevents-error-event
 */
final class ConsoleErrorEventListener extends AbstractEventListener
{

    /**
     * Constructor of the class
     *
     * @param LoggerInterface $logger
     */
    public function __construct(
        private LoggerInterface $logger,
    ) {
    }

    /**
     * {@inheritdoc}
     */
    public function getEventName() : string
    {
        return ConsoleEvents::ERROR;
    }

    /**
     * {@inheritdoc}
     *
     * @param ConsoleErrorEvent $event
     */
    public function handleEvent(Event $event) : void
    {
        $this->logger->error($event->getError()->getMessage(), [
            'error' => $event->getError(),
        ]);
    }
}
