<?php

declare(strict_types=1);

namespace App\Command;

use Psr\Container\ContainerInterface;
use Sunrise\Http\Factory\ServerRequestFactory;
use Sunrise\Http\Factory\StreamFactory;
use Sunrise\Http\Factory\UploadedFileFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * RoadRunner worker
 *
 * @link https://roadrunner.dev
 */
final class RoadRunnerWorkerCommand extends Command
{

    /**
     * {@inheritdoc}
     *
     * @param ContainerInterface $container
     */
    public function __construct(
        private ContainerInterface $container,
    ) {
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure() : void
    {
        $this->setName('roadrunner:worker');
        $this->setDescription('RoadRunner worker');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $router = $this->container->get('router');
        $doctrine = $this->container->get('doctrine');

        $worker = new \Spiral\RoadRunner\Http\PSR7Worker(
            \Spiral\RoadRunner\Worker::create(),
            new ServerRequestFactory(),
            new StreamFactory(),
            new UploadedFileFactory()
        );

        while ($request = $worker->waitRequest()) {
            $doctrine->getMaintainer()->reopenAllManagers();
            $worker->respond($router->run($request));
            $doctrine->getMaintainer()->clearAllManagers();
            $doctrine->getMaintainer()->closeAllConnections();
        }

        return 0;
    }
}
