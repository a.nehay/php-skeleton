<?php

declare(strict_types=1);

namespace App\Command;

use Psr\Container\ContainerInterface;
use Sunrise\Http\Router\Command\RouteListCommand as BaseRouteListCommand;
use Sunrise\Http\Router\Router;

/**
 * {@inheritdoc}
 */
final class RouteListCommand extends BaseRouteListCommand
{

    /**
     * {@inheritdoc}
     *
     * @param ContainerInterface $container
     */
    public function __construct(
        private ContainerInterface $container,
    ) {
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function getRouter() : Router
    {
        return $this->container->get('router');
    }
}
