<?php

declare(strict_types=1);

namespace App\Command;

use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use function file_get_contents;
use function rtrim;
use function strtr;

/**
 * Generates systemd unit file for RoadRunner worker
 *
 * @link https://wiki.debian.org/systemd/Services
 * @link https://manpages.debian.org/buster/systemd/systemd.service.5.en.html
 */
final class RoadRunnerWorkerGenerateSystemdUnitFileCommand extends Command
{

    /**
     * {@inheritdoc}
     *
     * @param ContainerInterface $container
     */
    public function __construct(
        private ContainerInterface $container,
    ) {
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure() : void
    {
        $this->setName('roadrunner:worker:generate-systemd-unit-file');
        $this->setDescription('Generates systemd unit file for RoadRunner worker');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $root = $this->container->get('app.root');

        // loads systemd unit template...
        $template = file_get_contents($root . '/.rr.service.stub');

        $replacements = [];
        $replacements['{cwd}'] = $root;
        $replacements['{user}'] = rtrim(`id -u -n`);
        $replacements['{group}'] = rtrim(`id -g -n`);
        $replacements['{rr}'] = rtrim(`which rr`);

        $output->writeln(strtr($template, $replacements));

        return 0;
    }
}
