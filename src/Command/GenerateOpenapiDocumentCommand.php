<?php

declare(strict_types=1);

namespace App\Command;

use Psr\Container\ContainerInterface;
use Sunrise\Http\Router\OpenApi\Command\GenerateOpenapiDocumentCommand as BaseGenerateOpenapiDocumentCommand;
use Sunrise\Http\Router\OpenApi\OpenApi;

/**
 * {@inheritdoc}
 */
final class GenerateOpenapiDocumentCommand extends BaseGenerateOpenapiDocumentCommand
{

    /**
     * {@inheritdoc}
     *
     * @param ContainerInterface $container
     */
    public function __construct(
        private ContainerInterface $container,
    ) {
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function getOpenapi() : OpenApi
    {
        return $this->container->get('openapi');
    }
}
