<?php

declare(strict_types=1);

namespace App\Middleware;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Sunrise\Http\Factory\ResponseFactory;
use Sunrise\Http\Header\HeaderRetryAfter;
use DateTime;

use function file_exists;

/**
 * AvailabilityMiddleware
 */
final class AvailabilityMiddleware implements MiddlewareInterface
{

    /**
     * Constructor of the class
     *
     * @param ContainerInterface $container
     */
    public function __construct(
        private ContainerInterface $container,
    ) {
    }

    /**
     * {@inheritdoc}
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler) : ResponseInterface
    {
        // if the file exists, then the application isn't available...
        $file = $this->container->get('app.root')  . '/.down';

        if (file_exists($file)) {
            return (new ResponseFactory)->createResponse(503)
                ->withHeader(...new HeaderRetryAfter(new DateTime('+5 minutes')));
        }

        return $handler->handle($request);
    }
}
