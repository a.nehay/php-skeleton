<?php

declare(strict_types=1);

namespace App\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

use function array_walk_recursive;
use function is_array;
use function is_object;
use function is_string;
use function trim;

/**
 * TrimStringsMiddleware
 */
final class TrimStringsMiddleware implements MiddlewareInterface
{

    /**
     * {@inheritdoc}
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler) : ResponseInterface
    {
        $data = $request->getParsedBody();

        if (!is_array($data) && !is_object($data)) {
            return $handler->handle($request);
        }

        array_walk_recursive($data, function (&$value) {
            $value = is_string($value) ? trim($value) : $value;
        });

        return $handler->handle($request->withParsedBody($data));
    }
}
