<?php

declare(strict_types=1);

namespace App\Middleware;

use Psr\Container\ContainerInterface;
use Sunrise\Http\Router\OpenApi\Middleware\RequestValidationMiddleware as BaseRequestValidationMiddleware;
use Sunrise\Http\Router\OpenApi\OpenApi;

/**
 * {@inheritdoc}
 */
final class RequestValidationMiddleware extends BaseRequestValidationMiddleware
{

    /**
     * {@inheritdoc}
     *
     * @param ContainerInterface $container
     */
    public function __construct(
        private ContainerInterface $container,
    ) {
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function getOpenapi() : OpenApi
    {
        return $this->container->get('openapi');
    }
}
