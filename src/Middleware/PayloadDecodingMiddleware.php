<?php

declare(strict_types=1);

namespace App\Middleware;

use App\Exception\UndecodablePayloadException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use JsonException;

use function json_decode;
use function sprintf;
use function strpos;
use function substr;
use function rtrim;

use const JSON_BIGINT_AS_STRING;
use const JSON_OBJECT_AS_ARRAY;
use const JSON_THROW_ON_ERROR;

/**
 * Payload decoding middleware
 */
final class PayloadDecodingMiddleware implements MiddlewareInterface
{

    /**
     * Maximum depth for JSON decoding process
     *
     * @var int
     */
    private const JSON_DECODING_MAX_DEPTH = 512;

    /**
     * JSON decoding options
     *
     * @link https://www.php.net/manual/ru/json.constants.php
     *
     * @var int
     */
    private const JSON_DECODING_OPTIONS = JSON_BIGINT_AS_STRING|JSON_OBJECT_AS_ARRAY|JSON_THROW_ON_ERROR;

    /**
     * {@inheritdoc}
     *
     * @throws UndecodablePayloadException
     *         If the request's body cannot be decoded.
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler) : ResponseInterface
    {
        $mediaType = $this->getMediaTypeFromRequest($request);

        if ('application/json' === $mediaType) {
            $payload = (string) $request->getBody();
            $data = $this->decodeJsonPayload($payload);
            $request = $request->withParsedBody($data);
        }

        return $handler->handle($request);
    }

    /**
     * Gets Media Type from the given request
     *
     * @link https://tools.ietf.org/html/rfc7231#section-3.1.1.1
     *
     * @param ServerRequestInterface $request
     *
     * @return string|null
     */
    private function getMediaTypeFromRequest(ServerRequestInterface $request) : ?string
    {
        if (!$request->hasHeader('Content-Type')) {
            return null;
        }

        // token "/" token *( OWS ";" OWS parameter )
        $mediaType = $request->getHeaderLine('Content-Type');

        // media type can contain parameters...
        $semicolon = strpos($mediaType, ';');

        if (false !== $semicolon) {
            $mediaType = substr($mediaType, 0, $semicolon);

            // rfc7231 allows spaces (see OWS) between media type and semicolon...
            $mediaType = rtrim($mediaType);
        }

        return $mediaType;
    }

    /**
     * Decodes the given JSON payload
     *
     * @param string $payload
     *
     * @return mixed
     *
     * @throws UndecodablePayloadException
     *         If the payload cannot be decoded.
     */
    private function decodeJsonPayload(string $payload) : mixed
    {
        try {
            return json_decode($payload, null, self::JSON_DECODING_MAX_DEPTH, self::JSON_DECODING_OPTIONS);
        } catch (JsonException $e) {
            throw new UndecodablePayloadException(sprintf('Invalid Payload: %s', $e->getMessage()), 0, $e);
        }
    }
}
