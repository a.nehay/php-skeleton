<?php

declare(strict_types=1);

namespace App\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Sunrise\Bridge\Doctrine\EntityManagerRegistry;

/**
 * CommitMiddleware
 */
final class CommitMiddleware implements MiddlewareInterface
{

    /**
     * Constructor of the class
     *
     * @param EntityManagerRegistry $doctrine
     */
    public function __construct(
        private EntityManagerRegistry $doctrine,
    ) {
    }

    /**
     * {@inheritdoc}
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler) : ResponseInterface
    {
        $response = $handler->handle($request);

        $this->doctrine->getManager('master')->flush();

        return $response;
    }
}
