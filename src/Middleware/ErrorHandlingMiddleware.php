<?php

declare(strict_types=1);

namespace App\Middleware;

use App\Dto\ErrorDto;
use App\Dto\ErrorsDto;
use App\Exception\AuthenticationFailedException;
use App\Exception\AuthenticationRequiredException;
use App\Exception\FileTooLargeException;
use App\Exception\UnprocessableEntityException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LoggerInterface;
use Sunrise\Http\Factory\ResponseFactory;
use Sunrise\Http\Header\HeaderAllow;
use Sunrise\Http\Router\Exception\BadRequestException;
use Sunrise\Http\Router\Exception\MethodNotAllowedException;
use Sunrise\Http\Router\Exception\PageNotFoundException;
use Sunrise\Http\Router\Exception\UnsupportedMediaTypeException;
use Throwable;

/**
 * Error handling middleware
 */
final class ErrorHandlingMiddleware implements MiddlewareInterface
{

    /**
     * Constructor of the class
     *
     * @param LoggerInterface $logger
     */
    public function __construct(
        private LoggerInterface $logger,
    ) {
    }

    /**
     * {@inheritdoc}
     */
    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler,
    ) : ResponseInterface {
        try {
            return $handler->handle($request);
        } catch (BadRequestException $e) {
            return $this->handleBadRequestException($request, $e);
        } catch (PageNotFoundException $e) {
            return $this->handlePageNotFoundException($request, $e);
        } catch (MethodNotAllowedException $e) {
            return $this->handleMethodNotAllowedException($request, $e);
        } catch (FileTooLargeException $e) {
            return $this->handleFileTooLargeException($request, $e);
        } catch (UnsupportedMediaTypeException $e) {
            return $this->handleUnsupportedMediaTypeException($request, $e);
        } catch (UnprocessableEntityException $e) {
            return $this->handleUnprocessableEntityException($request, $e);
        } catch (Throwable $e) {
            return $this->handleUnexpectedError($request, $e);
        }
    }

    /**
     * Handles Bad Request Exception
     *
     * @param ServerRequestInterface $request
     * @param BadRequestException $e
     *
     * @return ResponseInterface
     */
    private function handleBadRequestException(
        ServerRequestInterface $request,
        BadRequestException $e,
    ) : ResponseInterface {
        return (new ResponseFactory)->createJsonResponse(400, (new ErrorsDto)->addErrors($e->getErrors()));
    }

    /**
     * Handles Page Not Found Exception
     *
     * @param ServerRequestInterface $request
     * @param PageNotFoundException $e
     *
     * @return ResponseInterface
     */
    private function handlePageNotFoundException(
        ServerRequestInterface $request,
        PageNotFoundException $e,
    ) : ResponseInterface {
        return (new ResponseFactory)->createJsonResponse(404, (new ErrorsDto)->addError(new ErrorDto($e->getMessage())));
    }

    /**
     * Handles Method Not Allowed Exception
     *
     * @param ServerRequestInterface $request
     * @param MethodNotAllowedException $e
     *
     * @return ResponseInterface
     */
    private function handleMethodNotAllowedException(
        ServerRequestInterface $request,
        MethodNotAllowedException $e,
    ) : ResponseInterface {
        return (new ResponseFactory)->createJsonResponse(405, (new ErrorsDto)->addError(new ErrorDto($e->getMessage())))
            ->withHeader(...new HeaderAllow(...$e->getAllowedMethods()));
    }

    /**
     * Handles Payload Too Large Exception
     *
     * @param ServerRequestInterface $request
     * @param FileTooLargeException $e
     *
     * @return ResponseInterface
     */
    private function handleFileTooLargeException(
        ServerRequestInterface $request,
        FileTooLargeException $e,
    ) : ResponseInterface {
        return (new ResponseFactory)->createJsonResponse(413, (new ErrorsDto)->addError(new ErrorDto($e->getMessage())));
    }

    /**
     * Handles Unsupported Media Type Exception
     *
     * @param ServerRequestInterface $request
     * @param UnsupportedMediaTypeException $e
     *
     * @return ResponseInterface
     */
    private function handleUnsupportedMediaTypeException(
        ServerRequestInterface $request,
        UnsupportedMediaTypeException $e,
    ) : ResponseInterface {
        return (new ResponseFactory)->createJsonResponse(415, (new ErrorsDto)->addError(new ErrorDto($e->getMessage())))
            ->withHeader('Accept', $e->getJoinedSupportedTypes());
    }

    /**
     * Handles Unprocessable Entity Exception
     *
     * @param ServerRequestInterface $request
     * @param UnprocessableEntityException $e
     *
     * @return ResponseInterface
     */
    private function handleUnprocessableEntityException(
        ServerRequestInterface $request,
        UnprocessableEntityException $e,
    ) : ResponseInterface {
        return (new ResponseFactory)->createJsonResponse(422, (new ErrorsDto)->addErrors($e->getViolations()));
    }

    /**
     * Handles an unexpected error
     *
     * @param ServerRequestInterface $request
     * @param Throwable $e
     *
     * @return ResponseInterface
     */
    private function handleUnexpectedError(
        ServerRequestInterface $request,
        Throwable $e,
    ) : ResponseInterface {
        $this->logger->error($e->getMessage(), [
            'error' => $e,
        ]);

        return (new ResponseFactory)->createJsonResponse(500, (new ErrorsDto)
            ->addError(new ErrorDto(_('SOMETHING_WENT_WRONG'))));
    }
}
