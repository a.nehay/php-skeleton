<?php

declare(strict_types=1);

namespace App\Dto;

/**
 * @OpenApi\SchemaObject({
 *   "foo": @OpenApi\SchemaReference(".foo"),
 *   "bar": @OpenApi\SchemaReference(".bar"),
 * })
 */
final class TestDto
{

    /**
     * @OpenApi\Schema(
     *   type="string",
     * )
     */
    public string $foo;

    /**
     * @OpenApi\Schema(
     *   type="string",
     * )
     */
    public string $bar;
}
