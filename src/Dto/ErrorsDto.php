<?php

declare(strict_types=1);

namespace App\Dto;

use JsonSerializable;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Throwable;

use function is_array;

/**
 * @OpenApi\SchemaArray(
 *   @OpenApi\SchemaReference("ErrorDto")
 * )
 *
 * @OpenApi\Response(
 *   description="Something went wrong",
 *   content={
 *     "application/json": @OpenApi\MediaType(
 *       schema=@OpenApi\SchemaReference("ErrorsDto"),
 *     ),
 *   },
 * )
 *
 * @template T_Error as ErrorDto|ConstraintViolationInterface|Throwable|array
 */
final class ErrorsDto implements JsonSerializable
{

    /**
     * List of errors
     *
     * @var ErrorDto[]
     */
    private array $errors = [];

    /**
     * Adds the given error to the collection
     *
     * @param T_Error $error
     *
     * @return self
     */
    public function addError(ErrorDto|ConstraintViolationInterface|Throwable|array $error) : self
    {
        switch (true) {
            case $error instanceof ErrorDto:
                $this->errors[] = $error;
                break;

            case $error instanceof ConstraintViolationInterface:
                $this->errors[] = ErrorDto::fromViolation($error);
                break;

            case $error instanceof Throwable:
                $this->errors[] = ErrorDto::fromException($error);
                break;

            case is_array($error):
                $this->errors[] = ErrorDto::fromArray($error);
                break;
        }

        return $this;
    }

    /**
     * Adds the given errors to the collection
     *
     * @param iterable<T_Error> $errors
     *
     * @return self
     */
    public function addErrors(iterable $errors) : self
    {
        foreach ($errors as $error) {
            $this->addError($error);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize() : array
    {
        return [
            'errors' => $this->errors,
        ];
    }
}
