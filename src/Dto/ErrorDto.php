<?php

declare(strict_types=1);

namespace App\Dto;

use JsonSerializable;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Throwable;

use function is_string;
use function preg_replace;

/**
 * @OpenApi\SchemaObject({
 *   "message": @OpenApi\Schema(
 *     type="string",
 *   ),
 *   "code": @OpenApi\Schema(
 *     type="string",
 *     nullable=true,
 *   ),
 *   "property": @OpenApi\Schema(
 *     type="string",
 *     nullable=true,
 *   ),
 * })
 */
final class ErrorDto implements JsonSerializable
{

    /**
     * Constructor of the class
     *
     * @param string $message
     * @param string|null $code
     * @param string|null $property
     */
    public function __construct(
        private string $message,
        private ?string $code = null,
        private ?string $property = null,
    ) {
    }

    /**
     * Initializes the error from the given violation
     *
     * @param ConstraintViolationInterface $violation
     *
     * @return self
     */
    public static function fromViolation(ConstraintViolationInterface $violation) : self
    {
        $message = (string) $violation->getMessage();
        $code = $violation->getCode();
        $property = $violation->getPropertyPath();

        return new self($message, $code, $property);
    }

    /**
     * Initializes the error from the given exception
     *
     * @param Throwable $exception
     *
     * @return self
     */
    public static function fromException(Throwable $exception) : self
    {
        $message = $exception->getMessage();
        $code = (string) $exception->getCode();
        $property = null;

        return new self($message, $code, $property);
    }

    /**
     * Initializes the error from the given array
     *
     * @param array{
     *          message?: string,
     *          code?: string|null,
     *          property?: string|null,
     *        } $error
     *
     * @return self
     */
    public static function fromArray(array $error) : self
    {
        $message = 'Something went wrong';
        if (isset($error['message']) && is_string($error['message'])) {
            $message = $error['message'];
        }

        $code = null;
        if (isset($error['code']) && is_string($error['code'])) {
            $code = $error['code'];
        }

        $property = null;
        if (isset($error['property']) && is_string($error['property'])) {
            $property = $error['property'];
        }

        return new self($message, $code, $property);
    }

    /**
     * {@inheritdoc}
     *
     * @return array{
     *           message: string,
     *           code: string|null,
     *           property: string|null,
     *         }
     */
    public function jsonSerialize() : array
    {
        $property = $this->property;

        if (isset($property)) {
            // converts the following format: <foo.bar.baz>,
            // to the following format: <foo[bar][baz]>.
            //
            // that behaviour can help to work with HTML forms.
            $property = preg_replace('/\.([^.\133\135]+)/', '[$1]', $property);
        }

        return [
            'message' => $this->message,
            'code' => $this->code,
            'property' => $property,
        ];
    }
}
