<?php

declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * UnprocessableEntityException
 */
final class UnprocessableEntityException extends Exception
{

    /**
     * Constructor of the class
     *
     * @param ConstraintViolationListInterface $violations
     */
    public function __construct(
        private ConstraintViolationListInterface $violations,
    ) {
        parent::__construct('Unprocessable Entity');
    }

    /**
     * Gets the violations list
     *
     * @return ConstraintViolationListInterface
     */
    public function getViolations() : ConstraintViolationListInterface
    {
        return $this->violations;
    }
}
