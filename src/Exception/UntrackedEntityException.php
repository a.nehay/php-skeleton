<?php

declare(strict_types=1);

namespace App\Exception;

/**
 * UntrackedEntityException
 */
final class UntrackedEntityException extends Exception
{
}
