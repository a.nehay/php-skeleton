<?php

declare(strict_types=1);

namespace App\Exception;

use Sunrise\Http\Router\Exception\BadRequestException;

/**
 * UndecodablePayloadException
 */
final class UndecodablePayloadException extends BadRequestException implements ExceptionInterface
{
}
