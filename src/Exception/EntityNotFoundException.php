<?php

declare(strict_types=1);

namespace App\Exception;

use Sunrise\Http\Router\Exception\PageNotFoundException;

/**
 * EntityNotFoundException
 */
final class EntityNotFoundException extends PageNotFoundException implements ExceptionInterface
{
}
