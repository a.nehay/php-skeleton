<?php

declare(strict_types=1);

namespace App\Exception;

use Exception as BaseException;

/**
 * Exception
 */
class Exception extends BaseException implements ExceptionInterface
{
}
