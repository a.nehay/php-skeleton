<?php

declare(strict_types=1);

namespace App\Exception;

use Throwable;

/**
 * ExceptionInterface
 */
interface ExceptionInterface extends Throwable
{
}
