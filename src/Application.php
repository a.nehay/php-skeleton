<?php

declare(strict_types=1);

namespace App;

use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Application as ConsoleApplication;
use Symfony\Component\EventDispatcher\EventDispatcher;

use function file_exists;
use function filter_var;

use const FILTER_VALIDATE_BOOL;

/**
 * Application
 */
final class Application
{

    /**
     * The application's console application
     *
     * @var ConsoleApplication
     */
    private ConsoleApplication $consoleApplication;

    /**
     * The application's event dispatcher
     *
     * @var EventDispatcher
     */
    private EventDispatcher $eventDispatcher;

    /**
     * Constructor of the class
     *
     * @param ContainerInterface $container
     */
    public function __construct(
        private ContainerInterface $container,
    ) {
    }

    /**
     * Gets the application's console application
     *
     * @return ConsoleApplication
     */
    public function getConsoleApplication() : ConsoleApplication
    {
        if (!isset($this->consoleApplication)) {
            $this->consoleApplication = new ConsoleApplication($this->getName(), $this->getVersion());
            $this->consoleApplication->setDispatcher($this->getEventDispatcher());
            $this->consoleApplication->addCommands($this->container->get('commands'));
            $this->consoleApplication->addCommands($this->container->get('doctrine')->getCommands());
        }

        return $this->consoleApplication;
    }

    /**
     * Gets the application's event dispatcher
     *
     * @return EventDispatcher
     */
    public function getEventDispatcher() : EventDispatcher
    {
        if (!isset($this->eventDispatcher)) {
            $this->eventDispatcher = new EventDispatcher();
            foreach ($this->container->get('eventListeners') as $eventListener) {
                $this->eventDispatcher->addListener($eventListener->getEventName(), $eventListener);
            }
        }

        return $this->eventDispatcher;
    }

    /**
     * Gets the application root directory
     *
     * @return string
     */
    public function getRoot() : string
    {
        return $this->container->get('app.root');
    }

    /**
     * Gets the application name
     *
     * @return string
     */
    public function getName() : string
    {
        return $this->container->get('app.name');
    }

    /**
     * Gets the application version
     *
     * @return string
     */
    public function getVersion() : string
    {
        return $this->container->get('app.version');
    }

    /**
     * Gets the application URL
     *
     * @return string
     */
    public function getUrl() : string
    {
        return $this->container->get('app.url');
    }

    /**
     * Gets the application email address
     *
     * @return string
     */
    public function getEmail() : string
    {
        return $this->container->get('app.email');
    }

    /**
     * Gets the application environment name
     *
     * @return string
     */
    public function getEnv() : string
    {
        return $this->container->get('app.env');
    }

    /**
     * Checks if the application debug mode is enabled
     *
     * If the option was enabled, debug info will be logged.
     *
     * @return bool
     */
    public function isDebug() : bool
    {
        return filter_var($this->container->get('app.debug'), FILTER_VALIDATE_BOOL);
    }

    /**
     * Checks if the application silent mode is enabled
     *
     * If the option was enabled, fatal errors will not be displayed.
     *
     * @return bool
     */
    public function isSilent() : bool
    {
        return filter_var($this->container->get('app.silent'), FILTER_VALIDATE_BOOL);
    }

    /**
     * Checks if the application is down
     *
     * @return bool
     */
    public function isDown() : bool
    {
        return file_exists($this->getRoot() . '/.down');
    }

    /**
     * Checks if the application is running in the development environment
     *
     * @return bool
     */
    public function isDev() : bool
    {
        return 'dev' === $this->getEnv();
    }

    /**
     * Checks if the application is running in the production environment
     *
     * @return bool
     */
    public function isProd() : bool
    {
        return 'prod' === $this->getEnv();
    }

    /**
     * Checks if the application is running in the test environment
     *
     * @return bool
     */
    public function isTest() : bool
    {
        return 'test' === $this->getEnv();
    }

    /**
     * Runs the application
     *
     * @return void
     */
    public function run() : void
    {
        $this->getConsoleApplication()->run();
    }
}
