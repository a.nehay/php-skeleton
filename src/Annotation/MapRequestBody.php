<?php

declare(strict_types=1);

namespace App\Annotation;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS|Attribute::TARGET_METHOD)]
final class MapRequestBody
{

    /**
     * The annotation value
     *
     * @var class-string
     */
    public string $value;

    /**
     * Constructor of the class
     *
     * @param class-string $value
     */
    public function __construct(string $value)
    {
        $this->value = $value;
    }
}
