<?php

declare(strict_types=1);

namespace App\Controller;

use App\Annotation\MapRequestBody;
use App\Dto\TestDto;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Sunrise\Http\Factory\ResponseFactory;
use Sunrise\Http\Router\Annotation\Route;

/**
 * Test controller
 */
final class TestController
{

    /**
     * @OpenApi\Operation(
     *   requestBody=@OpenApi\RequestBody(
     *     content={
     *       "application/json": @OpenApi\MediaType(
     *         schema=@OpenApi\SchemaReference("App\Dto\TestDto"),
     *       ),
     *     },
     *   ),
     *   responses={
     *     200: @OpenApi\Response(
     *       description="OK",
     *       content={
     *         "application/json": @OpenApi\MediaType(
     *           schema=@OpenApi\SchemaObject({
     *             "data": @OpenApi\SchemaReference("App\Dto\TestDto"),
     *           }),
     *         ),
     *       },
     *     ),
     *     "default": @OpenApi\ResponseReference("App\Dto\ErrorsDto"),
     *   },
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    #[Route('test', path: '/test', method: 'POST')]
    #[MapRequestBody(TestDto::class)]
    public function test(ServerRequestInterface $request) : ResponseInterface
    {
        /** @var TestDto */
        $test = $request->getParsedBody();

        return (new ResponseFactory)->createJsonResponse(200, [
            'data' => $test,
        ]);
    }
}
