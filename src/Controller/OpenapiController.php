<?php

declare(strict_types=1);

namespace App\Controller;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Sunrise\Http\Factory\ResponseFactory;
use Sunrise\Http\Factory\StreamFactory;
use Sunrise\Http\Router\Annotation\Route;

/**
 * OpenAPI controller
 */
final class OpenapiController
{

    /**
     * Constructor of the class
     *
     * @param ContainerInterface $container
     */
    public function __construct(
        private ContainerInterface $container,
    ) {
    }

    /**
     * Swagger UI
     *
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    #[Route('swagger', path: '/swagger')]
    public function swagger(ServerRequestInterface $request) : ResponseInterface
    {
        $body = (new StreamFactory)->createStreamFromFile(
            $this->container->get('app.root') . '/resources/views/swagger.html'
        );

        return (new ResponseFactory)->createResponse(200)
            ->withHeader('Content-Type', 'text/html')
            ->withBody($body);
    }

    /**
     * OpenAPI document
     *
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    #[Route('openapi', path: '/openapi.json')]
    public function openapi(ServerRequestInterface $request) : ResponseInterface
    {
        $body = (new StreamFactory)->createStream(
            $this->container->get('openapi')->toJson()
        );

        return (new ResponseFactory)->createResponse(200)
            ->withHeader('Content-Type', 'application/json')
            ->withBody($body);
    }
}
