<?php

declare(strict_types=1);

use Symfony\Component\Dotenv\Dotenv;

require __DIR__ . '/../vendor/autoload.php';

(new Dotenv)->loadEnv(__DIR__ . '/../.env');

set_error_handler(function ($code, $text, $file, $line) {
    throw new ErrorException($text, 0, $code, $file, $line);
});
