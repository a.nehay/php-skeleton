<?php

declare(strict_types=1);

use DI\Container;
use DI\ContainerBuilder;

return (static function () : Container {
    $env = $_ENV['APP_ENV'] ?? 'dev';

    $builder = new ContainerBuilder();

    $builder->addDefinitions(
        ...glob(__DIR__ . '/definitions/*.php'),
        ...glob(__DIR__ . '/definitions/*.php.' . $env),
        ...glob(__DIR__ . '/definitions/*.php.local'),
    );

    if ('prod' === $env) {
        $builder->enableCompilation(__DIR__ . '/../var/cache/container');
    }

    return $builder->build();
})();
