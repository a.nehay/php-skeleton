<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Symfony\Component\Validator\ContainerConstraintValidatorFactory;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

use function DI\factory;

return [

    /**
     * The application validator
     *
     * @link https://symfony.com/doc/current/validation.html
     *
     * @var ValidatorInterface
     */
    ValidatorInterface::class => factory(function (ContainerInterface $container) : ValidatorInterface {
        return Validation::createValidatorBuilder()
            ->enableAnnotationMapping(true)
            ->addDefaultDoctrineAnnotationReader()
            ->setConstraintValidatorFactory(new ContainerConstraintValidatorFactory($container))
            ->getValidator();
    }),
];
