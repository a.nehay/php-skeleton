<?php

declare(strict_types=1);

use Doctrine\Persistence\ManagerRegistry;
use Sunrise\Bridge\Doctrine\EntityManagerRegistry;
use Symfony\Component\Cache\Adapter\ArrayAdapter;

use function DI\create;
use function DI\env;
use function DI\get;
use function DI\string;

return [
    'doctrine' => create(EntityManagerRegistry::class)
        ->constructor(
            get('doctrine.configuration'),
        ),

    'doctrine.configuration' => [
        'master' => [
            'dbal' => [
                'connection' => get('doctrine.configuration.master.dbal.connection'),
                'sql_logger' => get('doctrine.configuration.master.dbal.sql_logger'),
            ],
            'orm' => [
                'entity_locations' => get('doctrine.configuration.master.orm.entity_locations'),
                'entity_namespaces' => get('doctrine.configuration.master.orm.entity_namespaces'),
                'metadata_driver' => get('doctrine.configuration.master.orm.metadata_driver'),
                'metadata_cache' => get('doctrine.configuration.master.orm.metadata_cache'),
                'query_cache' => get('doctrine.configuration.master.orm.query_cache'),
                'result_cache' => get('doctrine.configuration.master.orm.result_cache'),
                'proxy_dir' => get('doctrine.configuration.master.orm.proxy_dir'),
                'proxy_auto_generate' => get('doctrine.configuration.master.orm.proxy_auto_generate'),
            ],
            'migrations' => [
                'migrations_paths' => get('doctrine.configuration.default.migrations.migrations_paths'),
                'logger' => get('doctrine.configuration.default.migrations.logger'),
            ],
            'types' => get('doctrine.configuration.types'),
        ],
    ],

    'doctrine.configuration.master.dbal.connection' => ['url' => env('DATABASE_MASTER_URL')],
    'doctrine.configuration.master.dbal.sql_logger' => get('doctrine.configuration.default.dbal.sql_logger'),
    'doctrine.configuration.master.orm.entity_locations' => get('doctrine.configuration.default.orm.entity_locations'),
    'doctrine.configuration.master.orm.entity_namespaces' => get('doctrine.configuration.default.orm.entity_namespaces'),
    'doctrine.configuration.master.orm.metadata_driver' => get('doctrine.configuration.default.orm.metadata_driver'),
    'doctrine.configuration.master.orm.metadata_cache' => get('doctrine.configuration.default.orm.metadata_cache'),
    'doctrine.configuration.master.orm.query_cache' => get('doctrine.configuration.default.orm.query_cache'),
    'doctrine.configuration.master.orm.result_cache' => get('doctrine.configuration.default.orm.result_cache'),
    'doctrine.configuration.master.orm.proxy_dir' => get('doctrine.configuration.default.orm.proxy_dir'),
    'doctrine.configuration.master.orm.proxy_auto_generate' => get('doctrine.configuration.default.orm.proxy_auto_generate'),

    'doctrine.configuration.default.dbal.sql_logger' => get('logger'),
    'doctrine.configuration.default.orm.entity_locations' => [string('{app.root}/src/Entity')],
    'doctrine.configuration.default.orm.entity_namespaces' => ['App' => 'App\Entity'],
    'doctrine.configuration.default.orm.metadata_driver' => 'attributes',
    'doctrine.configuration.default.orm.metadata_cache' => create(ArrayAdapter::class),
    'doctrine.configuration.default.orm.query_cache' => create(ArrayAdapter::class),
    'doctrine.configuration.default.orm.result_cache' => create(ArrayAdapter::class),
    'doctrine.configuration.default.orm.proxy_dir' => string('{app.root}/var/cache/doctrine/proxies'),
    'doctrine.configuration.default.orm.proxy_auto_generate' => true,

    'doctrine.configuration.default.migrations.logger' => get('logger'),
    'doctrine.configuration.default.migrations.migrations_paths' => [
        'App\Migrations' => string('{app.root}/resources/migrations'),
    ],

    'doctrine.configuration.types' => [
    ],

    // autowiring...
    ManagerRegistry::class => get('doctrine'),
    EntityManagerRegistry::class => get('doctrine'),
];
