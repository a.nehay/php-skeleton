<?php

declare(strict_types=1);

use App\Application;
use Psr\Container\ContainerInterface;
use Sunrise\Http\Router\Loader\DescriptorLoader;
use Sunrise\Http\Router\OpenApi\Object\Info;
use Sunrise\Http\Router\OpenApi\Object\SecurityScheme;
use Sunrise\Http\Router\OpenApi\Object\SecurityRequirement;
use Sunrise\Http\Router\OpenApi\Object\Server;
use Sunrise\Http\Router\OpenApi\OpenApi;
use Sunrise\Http\Router\RouteCollectionInterface;
use Sunrise\Http\Router\Router;

use function DI\factory;
use function DI\string;

return [

    /**
     * The application routes
     *
     * @var RouteCollectionInterface
     */
    'routes' => factory(function (ContainerInterface $container) : RouteCollectionInterface {
        $loader = new DescriptorLoader();
        $loader->setContainer($container);
        $loader->setCache($container->get('routes.cache'));
        $loader->attachArray($container->get('routes.locations'));

        return $loader->load();
    }),

    /**
     * The application router
     *
     * @var Router
     */
    'router' => factory(function (ContainerInterface $container, Application $application) : Router {
        $router = new Router();
        $router->addPatterns($container->get('router.patterns'));
        $router->addHosts($container->get('router.hosts'));
        $router->addRoute(...$container->get('routes')->all());
        $router->addMiddleware(...$container->get('middlewares'));
        $router->setEventDispatcher($application->getEventDispatcher());

        return $router;
    }),

    /**
     * The application openapi
     *
     * @var OpenApi
     */
    'openapi' => factory(function (ContainerInterface $container) : OpenApi {
        $info = new Info(
            $container->get('app.name'),
            $container->get('app.version')
        );

        $openapi = new OpenApi($info);
        $openapi->addRoute(...$container->get('routes')->all());
        $openapi->setCache($container->get('openapi.cache'));

        // https://swagger.io/docs/specification/api-host-and-base-path/
        $server = new Server($container->get('app.url'));
        $openapi->addServer($server);

        // https://swagger.io/docs/specification/authentication/bearer-authentication/
        $securityScheme = new SecurityScheme('bearerAuth', 'http');
        $securityScheme->setScheme('bearer');
        $openapi->addComponent($securityScheme);
        $securityRequirement = new SecurityRequirement('bearerAuth');
        $openapi->addSecurityRequirement($securityRequirement);

        return $openapi;
    }),

    /**
     * Routes cache
     *
     * @var \Psr\SimpleCache\CacheInterface
     */
    'routes.cache' => null,

    /**
     * Routes locations
     *
     * @var array<string>
     */
    'routes.locations' => [
        string('{app.root}/src/Controller'),
    ],

    /**
     * The router hosts (host table)
     *
     * @var array<string, string[]>
     */
    'router.hosts' => [
        'local' => ['127.0.0.1', 'localhost'],
    ],

    /**
     * The router patterns
     *
     * @var array<string, string>
     */
    'router.patterns' => [
    ],

    /**
     * The openapi cache
     *
     * @var \Psr\SimpleCache\CacheInterface
     */
    'openapi.cache' => null,
];
