<?php

declare(strict_types=1);

use Monolog\Handler\ErrorLogHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

use function DI\create;
use function DI\get;
use function DI\string;

return [

    /**
     * The application logger
     *
     * @link https://seldaek.github.io/
     *
     * @var LoggerInterface
     */
    'logger' => create(Logger::class)
        ->constructor(
            get('monolog.configuration.name'),
            get('monolog.configuration.handlers'),
            get('monolog.configuration.processors'),
        ),

    /**
     * The logger name
     *
     * @var string
     */
    'monolog.configuration.name' => string('{app.name}@{app.version}-{app.env}'),

    /**
     * The logger handlers
     *
     * @link https://seldaek.github.io/monolog/doc/02-handlers-formatters-processors.html#handlers
     *
     * @var \Monolog\Handler\HandlerInterface[]
     */
    'monolog.configuration.handlers' => [
        create(ErrorLogHandler::class),
    ],

    /**
     * The logger processors
     *
     * @link https://seldaek.github.io/monolog/doc/02-handlers-formatters-processors.html#processors
     *
     * @var \Monolog\Processor\ProcessorInterface[]
     */
    'monolog.configuration.processors' => [
    ],

    // autowiring...
    LoggerInterface::class => get('logger'),
];
