<?php

declare(strict_types=1);

use function DI\autowire;
use function DI\create;
use function DI\env;

return [

    /**
     * The application root directory
     *
     * @var string
     */
    'app.root' => realpath(__DIR__ . '/../..'),

    /**
     * The application name
     *
     * @var string
     */
    'app.name' => 'acme',

    /**
     * The application summary
     *
     * @var string
     */
    'app.summary' => 'Acme Corporation',

    /**
     * The application version
     *
     * @link https://semver.org/
     *
     * @var string
     */
    'app.version' => '1.0.0',

    /**
     * The application environment
     *
     * Use the following values: "dev" or "prod".
     *
     * Note that the test environment will be detected automatically.
     *
     * @var string
     */
    'app.env' => env('APP_ENV', 'dev'),

    /**
     * Indicates whether the application debug mode is enabled
     *
     * If the option is enabled, debug info will be logged.
     *
     * Use the following values: "1", "on", "true", "yes", or any other value for false.
     *
     * @var string
     */
    'app.debug' => env('APP_DEBUG', 'on'),

    /**
     * Indicates whether the application silent mode is enabled
     *
     * If the option is enabled, fatal errors will not be displayed.
     *
     * Use the following values: "1", "on", "true", "yes", or any other value for false.
     *
     * @var string
     */
    'app.silent' => env('APP_SILENT', 'off'),

    /**
     * The application URL
     *
     * Use the following format: scheme://host(:port)
     *
     * Note that the standard port cannot be specified.
     *
     * @var string
     */
    'app.url' => env('APP_URL', 'http://localhost'),

    /**
     * The application email address
     *
     * This email address may be provided for feedback.
     *
     * @var string
     */
    'app.email' => env('APP_EMAIL', 'webmaster@localhost'),

    /**
     * The application commands
     *
     * @var \Symfony\Component\Console\Command\Command[]
     */
    'commands' => [
        autowire(App\Command\GenerateJsonSchemaCommand::class),
        autowire(App\Command\GenerateOpenapiDocumentCommand::class),
        autowire(App\Command\RoadRunnerWorkerCommand::class),
        autowire(App\Command\RoadRunnerWorkerGenerateSystemdUnitFileCommand::class),
        autowire(App\Command\RouteListCommand::class),
    ],

    /**
     * The application middlewares
     *
     * @var \Psr\Http\Server\MiddlewareInterface[]
     */
    'middlewares' => [
        autowire(App\Middleware\ErrorHandlingMiddleware::class),
        autowire(App\Middleware\AvailabilityMiddleware::class),
        autowire(App\Bundle\Cors\CorsMiddleware::class),
        create(App\Middleware\TrimStringsMiddleware::class),
        create(App\Middleware\PayloadDecodingMiddleware::class),
        autowire(App\Middleware\CommitMiddleware::class),
    ],

    /**
     * The application's event listeners
     *
     * @var \App\EventListener\AbstractEventListener[]
     */
    'eventListeners' => [
        autowire(App\EventListener\ConsoleCommandEventListener::class),
        autowire(App\EventListener\ConsoleErrorEventListener::class),
        autowire(App\EventListener\ConsoleSignalEventListener::class),
        autowire(App\EventListener\ConsoleTerminateEventListener::class),
        create(App\EventListener\RouterRouteEventListener::class),
    ],
];
