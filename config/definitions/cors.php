<?php

declare(strict_types=1);

use App\Bundle\Cors\CorsConfiguration;

use function DI\create;
use function DI\get;

return [

    /**
     * CORS configuration
     *
     * @var CorsConfiguration
     */
    CorsConfiguration::class => create()
        ->constructor(
            get('cors.allow_credentials'),
            get('cors.allowed_headers'),
            get('cors.allowed_methods'),
            get('cors.allowed_origins'),
            get('cors.max_age'),
        ),

    /**
     * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Credentials
     *
     * @var bool
     */
    'cors.allow_credentials' => true,

    /**
     * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Headers
     *
     * @var string[]
     */
    'cors.allowed_headers' => ['*'],

    /**
     * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Methods
     *
     * @var string[]
     */
    'cors.allowed_methods' => ['*'],

    /**
     * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Origin
     *
     * @var string[]
     */
    'cors.allowed_origins' => ['*'],

    /**
     * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Max-Age
     *
     * @var int
     */
    'cors.max_age' => -1,
];
